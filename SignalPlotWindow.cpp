﻿/*Copyright (C) 2019  Vlad Vasiliu

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/
#include "SignalPlotWindow.h"
#include "qosc.h"
#include "ui_SignalPlotWindow.h"

SignalPlotWindow::SignalPlotWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    connect(&ConnectionRouter,&qRouter::Log,this,&SignalPlotWindow::Logger);


    connect(&config,&qConfig::Log,this,&SignalPlotWindow::Logger);
    connect(&config,&qConfig::SetVariable,this,&SignalPlotWindow::SetVariable);
    connect(&config,&qConfig::SetElf,this,&SignalPlotWindow::SetElf);

    ui->varlist->setContextMenuPolicy(Qt::CustomContextMenu);
    ui->measurement->setContextMenuPolicy(Qt::CustomContextMenu);


    connect(&Interpreter,&qInterpreter::Log,this,&SignalPlotWindow::Logger);
    connect(&Interpreter,&qInterpreter::SetValue,this,&SignalPlotWindow::SetVarValue);
    connect(&Interpreter,&qInterpreter::GetValue,this,&SignalPlotWindow::GetVarValue);

    connect(&GuiTimer,&QTimer::timeout,this,&SignalPlotWindow::updateValues);

}
void SignalPlotWindow::updateLayout(int nrCol)
{
    int i=0;

    // ---- Check if there are any widgets to update -----
    if(qOscViewList.length()>0)
    {
        // ---- Cleanup the layout ----
        for( i=0;i < qOscViewList.length();i++)
        {
            ui->MeasurementLayout->removeWidget((QWidget*)&qOscViewList[i]);
        }

        // ---- Setup the layout ----
        for(i=0;i < qOscViewList.length()-1;i++)
        {
            ui->MeasurementLayout->addWidget((QWidget*)&qOscViewList[i],i/nrCol,i%nrCol);
        }

        ui->MeasurementLayout->addWidget((QWidget*)&qOscViewList[i],i/nrCol,i%nrCol,1,nrCol-(i%nrCol));
    }
}
void SignalPlotWindow::ConnectToNewOscView(qVariable &var)
{
    qOsc MainQosc;
    // ---- Add a new Osc view in the list ----
    qOscViewList.append(MainQosc);

    if(qOscViewList.length() > 0)
    {
        qOscViewList.last().setInteractions(QCP::iRangeDrag | QCP::iRangeZoom | QCP::iSelectPlottables |QCP::iSelectItems);

        updateLayout(Meas_ColNr);

        qOscViewList.last().setParent(ui->measurement);

        // ---- Conect the signals to the new Oscilscope -----
        connect(&GuiTimer,&QTimer::timeout,&qOscViewList.last(),&qOsc::update);
        connect(&var,&qVariable::displayData,&qOscViewList.last(),&qOsc::displayData);
        connect(&qOscViewList.last(),&qOsc::customContextMenuRequested,this,&SignalPlotWindow::on_qOscContextMenuRequestedSlot);

        qOscViewList.last().setContextMenuPolicy(Qt::CustomContextMenu);
        var.triggerDisplayData();
        qOscViewList.last().update();

        OscActiveView = qOscViewList.length() - 1;
    }
}
void SignalPlotWindow::DisconnectFromOscView(qVariable &var)
{
    for(int count = 0;count<qOscViewList.length();count++)
    {
        if(qOscViewList[count].getSignals().contains(var.Name))
        {
            disconnect(&var,&qVariable::displayData,&qOscViewList[count],&qOsc::displayData);
        }
    }
}
void SignalPlotWindow::RemoveSelectedOscView()
{
    qOscViewList.removeAt(OscActiveView);
    OscActiveView=0;
}
void SignalPlotWindow::ConnectToOscView(qVariable &var)
{

    if(qOscViewList.length() > 0)
    {
        connect(&var,&qVariable::displayData,&qOscViewList[OscActiveView],&qOsc::displayData);
    }
    else
    {
        ConnectToNewOscView(var);
    }
}
void SignalPlotWindow::ResetOscViews()
{
    for(int count = 0;count<qOscViewList.length();count++)
    {
        qOscViewList[count].resetall();
    }
}
SignalPlotWindow::~SignalPlotWindow()
{
    delete ui;
}
void SignalPlotWindow::Logger(QString text)
{
    if(!text.contains("DEBUG"))
    {
        if(!text.contains("ERROR :"))
        {
            ui->Terminal->append("<p style=\"background-color:White\">"+text+"</p>");
        }
        else
        {
            ui->Terminal->append("<p style=\"background-color:Tomato\">"+text+"</p>");
        }

    }
}

void SignalPlotWindow::on_actionUse_Elf_triggered()
{
    QString filename  = QFileDialog::getOpenFileName(this,"Select Elf File","","*");

    if(filename.size()!=0)
    {
        Logger("INFO : Elf File : "+filename+" was added");
        elffile= filename;
        emit ElfUpdate(elffile);
    }
    else
    {
        Logger("ERROR : No Elf file was selected");
    }
}


void SignalPlotWindow::on_actionOpen_triggered()
{
    QString filename  = QFileDialog::getOpenFileName(this,"Select Project File","","*.json");

    if(filename.size()!=0)
    {
        Logger("INFO : Project File : "+filename+" was selected");
        config.readConfig(filename);
    }
    else
    {
        Logger("ERROR : No Project file was selected");
    }
}

bool SignalPlotWindow::isVariableInputOk(QString &name,int &type,uint64_t &ui64addr,double &flgain,double &floffset)
{
    bool ok= false;

    QString address;
    QString gain;
    QString offset;

    name    = ui->VariableName->text().simplified();
    type    = ui->VariableType->currentIndex();
    address = ui->VariableAddress->text().simplified();
    gain    = ui->VariableGain->text().simplified();
    offset  = ui->VariableOffset->text().simplified();


    // ---- Check the input selection. Ignore Address and type because it can be updated using the Elf file -----
    if((name.size()>0)&&(gain.size()>0)&&(offset.size()>0))
    {
        bool ok1 = false;
        bool ok2 = false;
        bool ok3 = false;
        ui64addr = address.toLong(&ok1,16);
        flgain   = gain.toDouble(&ok2);
        floffset = offset.toDouble(&ok3);

        if(ok1 == false)
        {
            Logger("ERROR : Error while creating a variable . Address is not a valid hexadecimal value");
        }
        else if(ok2 == false)
        {
            Logger("ERROR : Error while creating a variable . Gain is not a float value (or a double)");
        }else if(ok3 == false)
        {
            Logger("ERROR : Error while creating a variable . Offset is not a float value (or a double)");
        }
        else
        {
            ok = true;
        }
    }
    return ok;
}

bool SignalPlotWindow::isVarInMeasurementList(QString &name)
{
    bool exists=false;

    // ---- Search in the current list for duplicates ----
    if(ui->varlist->findItems(name,Qt::MatchFlags::enum_type::MatchExactly).length()> 0)
    {
        Logger("INFO : Variable " + name + " is already in the list !");
        exists = true;
    }

    return exists;
}

void SignalPlotWindow::on_Add_Variable_clicked()
{
    QString  name;
    int      type=0;
    uint64_t address=0;
    double   gain=0;
    double   offset=0;

    if(isVariableInputOk(name,type,address,gain,offset))
    {
        SetVariable(name, type,address,gain,offset);
    }
    else
    {
        Logger("ERROR : Error while creating a variable . Some fields are missing !");
    }

}
void SignalPlotWindow:: SetElf(QString data)
{
    elffile = data;
}

void SignalPlotWindow::SetVariable(QString name,int type,uint64_t address,double gain,double offset)
{
    // ---- Search in the current list for duplicates ----
    if(!isVarInMeasurementList(name))
    {
        // ---- Add to the variable list -----
        VariableList.append(qVariable(name,address,(qVariableType)type,gain,offset));

        // ---- Connect the signals to the newly added member ----
        connect(this,&SignalPlotWindow::ElfUpdate,&VariableList.last(),&qVariable::updateElfInformation);
        connect(this,&SignalPlotWindow::resetData,&VariableList.last(),&qVariable::resetBuffers);

        connect(&VariableList.last(),&qVariable::Log,this,&SignalPlotWindow::Logger);

        connect(&VariableList.last(),&qVariable::requestRawData,&ConnectionRouter,&qRouter::ReadReq);
        connect(&VariableList.last(),&qVariable::valueChanged,&ConnectionRouter,&qRouter::Write);

        connect(&ConnectionRouter,&qRouter::updateRawData,&VariableList.last(),&qVariable::updateRawData);
        connect(&ConnectionRouter,&qRouter::resetData,&VariableList.last(),&qVariable::resetBuffers);

        connect(&EventTrigger,&QTimer::timeout,&VariableList.last(),&qVariable::requestData);




        // ---- Emit an elf update ----
        if(elffile.size() > 0)
        {
            emit ElfUpdate(elffile);
        }
        // ---- Update the fields according with the update ----
        updateVariableInput(VariableList.last().Name);
        // ---- Update the variable buffer size ----
        VariableList.last().ReceiveMaxBufferSize = calculateBufferSize();

        Logger("INFO : Variable "+name+" was added!");

        ui->varlist->addTopLevelItem(new QTreeWidgetItem({name,"---"}));
    }
}



void SignalPlotWindow::on_GetFromElf_clicked()
{
    if(elffile.size() > 0)
    {
        emit ElfUpdate(elffile);
    }
    else
    {
        ui->Terminal->append("ERROR : No Elf file was selected. Please select an Elf File !");
    }
}



void SignalPlotWindow::updateVariableInput (QString name)
{
    for(int count=0;count<VariableList.size();count++)
    {
        if(name == VariableList[count].Name)
        {
            ui->VariableName->setText(VariableList[count].Name);
            ui->VariableAddress->setText(QString("%1").arg(VariableList[count].Address,0,16));
            ui->VariableType->setCurrentIndex((int)VariableList[count].Type);
            ui->VariableGain->setText(QString("%1").arg(VariableList[count].Gain));
            ui->VariableOffset->setText(QString("%1").arg(VariableList[count].Offset));
            break;
        }
    }
}

void SignalPlotWindow::AddMeasurement(int view)
{
    foreach(auto item,ui->varlist->selectedItems())
    {

        for(int cnt=0;cnt < VariableList.length();cnt++)
        {
            if(VariableList[cnt].Name == item->text(0))
            {
                // ---- Make the selected view Active----
                OscActiveView = view;
                Logger("INFO : Variable "+VariableList[cnt].Name+" was added in the measurement view");
                ConnectToOscView(VariableList[cnt]);
                break;
            }
        }

    }
}
void SignalPlotWindow::AddMeasurementNewAxis()
{
    foreach(auto item,ui->varlist->selectedItems())
    {

        for(int cnt=0;cnt < VariableList.length();cnt++)
        {
            if(VariableList[cnt].Name == item->text(0))
            {
                Logger("INFO : Variable "+VariableList[cnt].Name+" was added in the new measurement view");
                ConnectToNewOscView(VariableList[cnt]);
                break;
            }
        }

    }
}
void SignalPlotWindow::RemoveMeasurement()
{
    foreach(auto item,ui->varlist->selectedItems())
    {
        for(int cnt=0;cnt < VariableList.length();cnt++)
        {
            if(VariableList[cnt].Name == item->text(0))
            {
                Logger("INFO : Variable "+VariableList[cnt].Name+"was removed from the Measurement view");
                DisconnectFromOscView(VariableList[cnt]);
                break;
            }
        }
    }
}
void SignalPlotWindow::RemoveGraph()
{
    if(qOscViewList.length()>0)
    {
        qOscViewList.removeAt(OscActiveView);
        OscActiveView=0;
        updateLayout(Meas_ColNr);
    }
}

void SignalPlotWindow::FitGraph()
{
    if(qOscViewList.length()>0)
    {
        qOscViewList[OscActiveView].fitall();
    }
}

void SignalPlotWindow::on_UpdateVar_clicked()
{
    QString  name;
    int      type=0;
    uint64_t address=0;
    double   gain=0;
    double   offset=0;

    if(isVariableInputOk(name,type,address,gain,offset))
    {
        for(int count=0;count<VariableList.size();count++)
        {
            if(name == VariableList[count].Name)
            {
                VariableList[count].Address = address;
                VariableList[count].Gain    = gain;
                VariableList[count].Offset  = offset;
                VariableList[count].Type    = (qVariableType)type;
                break;
            }
        }
    }
}


void SignalPlotWindow::on_RemoveVar_clicked()
{
    QTreeWidgetItem * item;

    // ---- Check if there is a selected item ----
    if(ui->varlist->selectedItems().size() == 0) {return;}

    // ---- Take only the first element even if are more selected ----
    item = ui->varlist->selectedItems().first();

    // ---- Search in the measurement list the Variable Name ----
    for(int count=0;count<VariableList.size();count++)
    {
        if(item->text(0) == VariableList[count].Name)
        {
            // ---- disconnect the signals from the variable ----

            disconnect(this,&SignalPlotWindow::ElfUpdate,&VariableList[count],&qVariable::updateElfInformation);
            disconnect(this,&SignalPlotWindow::resetData,&VariableList[count],&qVariable::resetBuffers);

            disconnect(&VariableList[count],&qVariable::Log,this,&SignalPlotWindow::Logger);
            DisconnectFromOscView(VariableList[count]);
            disconnect(&VariableList[count],&qVariable::requestRawData,&ConnectionRouter,&qRouter::ReadReq);
            disconnect(&VariableList[count],&qVariable::valueChanged,&ConnectionRouter,&qRouter::Write);

            disconnect(&ConnectionRouter,&qRouter::updateRawData,&VariableList[count],&qVariable::updateRawData);
            disconnect(&ConnectionRouter,&qRouter::resetData,&VariableList[count],&qVariable::resetBuffers);

            disconnect(&EventTrigger,&QTimer::timeout,&VariableList[count],&qVariable::requestData);
            VariableList.removeAt(count);
            break;
        }
    }
    // ---- Delete Entry ----
    ui->varlist->takeTopLevelItem(ui->varlist->indexOfTopLevelItem(item));
}


void SignalPlotWindow::on_OpenOCDConnect_clicked()
{
    ConnectionRouter.Connect(ui->OpenOCD_Port->text(),(qRouter::tenConnectionType)(ui->ConnectionMethod->currentIndex()+1));
   startTimers();
}


void SignalPlotWindow::on_OpenOCDDisconnect_clicked()
{
    stopTimers();
    ConnectionRouter.Disconnect();
}


void SignalPlotWindow::startTimers()
{

    emit resetData();
    ResetOscViews();
    EventTrigger.setInterval(ui->TriggerPeriod->text().toDouble()*1000);
    EventTrigger.start(ui->TriggerPeriod->text().toDouble()*1000);
    GuiTimer.setInterval(100);
    GuiTimer.start(100);

}


void SignalPlotWindow::stopTimers()
{
    EventTrigger.stop();
    GuiTimer.stop();
}

void SignalPlotWindow::updateValues()
{
    QList<QTreeWidgetItem*> items = ui->varlist->findItems("",Qt::MatchFlags::enum_type::MatchContains,0);

    foreach(auto item,items)
    {
        for(int count=0;count < VariableList.length();count++)
        {
           item->setText(1,QString("%1").arg(GetVarValue(item->text(0))));
        }
    }
}


int SignalPlotWindow::calculateBufferSize()
{
    double sampletime = ui->TriggerPeriod->text().toDouble();
    double val = 1;

    if(sampletime > 0)
    {
        // ---- Calculate the needed buffersize and update the UI acordingly ----
        val = ui->BufferSizeInput->text().toDouble()/sampletime;
        val = (val<0)? 0:val;
        val = (val>(double)std::numeric_limits<int>::max())? (double)std::numeric_limits<int>::max():val;

        ui->BufferSizeInput->setText(QString("%1").arg(val*sampletime));

        Logger("INFO : Buffer Size set to "+QString("%1").arg((int)round(val))+" points");

        if(val>10000)
        {
            Logger("WARNING : The buffer size is bigger than 10000 points for each signal. This can affect the performance !");
        }
    }
    else
    {
        Logger("ERROR : Cannot calculate the buffer size ! (Check the sample time)!");
    }
    return (int)round(val);
}

void SignalPlotWindow::on_BufferSizeInput_editingFinished()
{
    if(!VariableList.empty())
    {
        int val = calculateBufferSize();

        for(int count=0;count<VariableList.size();count++)
        {
            VariableList[count].ReceiveMaxBufferSize = val;
        }
    }
}



void SignalPlotWindow::on_actionUpdate_Addresses_triggered()
{
    emit ElfUpdate(elffile);
}


void SignalPlotWindow::on_actionSave_triggered()
{
    QString filename  = QFileDialog::getSaveFileName(this,"Select Project File","","*.json");

    if(filename.size()!=0)
    {
        if(!filename.contains(".json"))
        {
            filename +=".json";
        }
        config.writeElfPath(elffile);

        for(int cnt = 0;cnt <VariableList.size();cnt++)
        {
            config.writeVariable(VariableList[cnt]);
        }

        config.writeConfig(filename);

        Logger("INFO : Json File : "+filename+" was saved");

    }
    else
    {
        Logger("ERROR : No Json file was selected");
    }

}


void SignalPlotWindow::on_actionNew_triggered()
{
    stopTimers();
    on_OpenOCDDisconnect_clicked();
    emit resetData();
    ResetOscViews();
    VariableList.clear();
    ui->varlist->clear();
    ui->VariableName->setText("");
    ui->VariableAddress->setText("0");
    ui->VariableType->setCurrentIndex(0);
    ui->VariableGain->setText("1");
    ui->VariableOffset->setText("0");
    ui->Terminal->clear();
    qOscViewList.clear();
    this->repaint();
}


void SignalPlotWindow::on_varlist_customContextMenuRequested(const QPoint &pos)
{
    QMenu menu(this);



    if(qOscViewList.length() > 0)
    {
        menu.addAction("Add to last added figure",[=](){this->AddMeasurement(qOscViewList.length()-1);});
        menu.addAction("Add to new figure",this,&SignalPlotWindow::AddMeasurementNewAxis);
        menu.addSeparator();

        for(int count=0;count<qOscViewList.length();count++)
        {
            menu.addAction(QString("Add to the figure %1").arg(count),[=](){this->AddMeasurement(count);});
        }
    }
    else
    {
        menu.addAction("Add to new figure",this,&SignalPlotWindow::AddMeasurementNewAxis);
    }
    menu.addSeparator();
    menu.addAction("Remove from all figures",this,&SignalPlotWindow::RemoveMeasurement);

    menu.show();
    menu.exec(ui->varlist->mapToGlobal(pos));
}
void SignalPlotWindow::on_qOscContextMenuRequestedSlot(const QPoint &pos)
{
    QMenu menu(this);
    int cnt=0;

    for(cnt=0;cnt<qOscViewList.length();cnt++)
    {
        if(qOscViewList[cnt].underMouse())
        {
            OscActiveView = cnt;
            break;
        }
    }

    menu.addAction("Fit all",this,&SignalPlotWindow::FitGraph);
    menu.addAction("RemoveGraph",this,&SignalPlotWindow::RemoveGraph);

    menu.show();
    menu.exec(qOscViewList[cnt].mapToGlobal(pos));
}

void SignalPlotWindow::SetVarValue(QString variable,double data)
{
    for(int count=0;count < VariableList.length();count++)
    {
        if(VariableList[count].Name == variable)
        {
            VariableList[count].setValue(data);
            return;
        }
    }

    Logger("ERROR : Varaible "+variable+" was not found in the variable list");
}
double SignalPlotWindow::GetVarValue(QString variable)
{
    if(!EventTrigger.isActive())
    {
        Logger("ERROR : Measurement is not active ");
        return 0;
    }

    for(int count=0;count < VariableList.length();count++)
    {
        if(VariableList[count].Name == variable)
        {
            return VariableList[count].getValue();
        }
    }
    Logger("ERROR : Varaible "+variable+" was not found in the variable list");
    return 0;
}


void SignalPlotWindow::on_Cmd_editingFinished()
{
}


void SignalPlotWindow::on_actionSave_Data_as_CSV_triggered()
{

    QString filename  = QFileDialog::getSaveFileName(this,"Select CSV File Pattern","","*.csv");

    if(filename.size()!=0)
    {

        for(int cnt = 0;cnt <VariableList.size();cnt++)
        {
            QString name = VariableList[cnt].Name;

            QFile save_file(filename+"_"+name.replace('.','_')+".csv");

            if(!save_file.open(QIODevice::WriteOnly))
            {
                Logger("ERROR : Error during saving : "+filename);
                return;
            }

            save_file.write(QString(VariableList[cnt].Name+"_X\t"+VariableList[cnt].Name+"_Y\n").toUtf8());

            for(int count=0;count<VariableList[cnt].ReadValues.length();count++)
            {
                save_file.write(QString(QString("%1").arg(VariableList[cnt].TStampValues[count])+"\t"+QString("%1").arg(VariableList[cnt].ReadValues[count])+"\n").toUtf8());
            }

            save_file.close();
            Logger("INFO : CSV File : "+save_file.fileName()+" was saved");
        }
    }
    else
    {
        Logger("ERROR : No CSV file pattern was selected");
    }
}


void SignalPlotWindow::on_TriggerPeriod_editingFinished()
{
    // ---- Since the sampling time will affect the buffer size recalculate the Buffer size in points ----
    on_BufferSizeInput_editingFinished();
    EventTrigger.setInterval(ui->TriggerPeriod->text().toDouble()*1000);
}


void SignalPlotWindow::on_ConnectionMethod_currentIndexChanged(int index)
{
    switch(index)
    {
    case 0 : {ui->OpenOCD_Port->setText("4444");}break;
    case 1 : {ui->OpenOCD_Port->setText("9999");}break;
    case 2 : {ui->OpenOCD_Port->setText("/dev/ttyUSB0");}break;
    case 3 : {ui->OpenOCD_Port->setText("can0");}break;
    }
}



void SignalPlotWindow::on_varlist_itemClicked(QTreeWidgetItem *item, int column)
{
    // ---- Update the Variable Input based on the VariableList ----
    updateVariableInput(item->text(0));
    //---- Add the variable in the command line ----
    ui->Cmd->setText(item->text(0));

    Q_UNUSED(column);
}


void SignalPlotWindow::on_Cmd_returnPressed()
{
    Interpreter.Decode(ui->Cmd->text());
}

