/*Copyright (C) 2019  Vlad Vasiliu

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/
#include "qvariable.h"
#include <QProcess>

qVariable::qVariable(QObject *parent) : QObject(parent)
{

}

qVariable::qVariable(QString &name, uint64_t address, qVariableType type, double gain, double offset):Name(name),Address(address),Type(type),Gain(gain),Offset(offset)
{
    switch(type)
    {
    case qVarUint8: {SizeInBytes = 1u;}break;
    case qVarSint8: {SizeInBytes = 1u;}break;
    case qVarUint16:{SizeInBytes = 2u;}break;
    case qVarSint16:{SizeInBytes = 2u;}break;
    case qVarUint32:{SizeInBytes = 4u;}break;
    case qVarSint32:{SizeInBytes = 4u;}break;
    case qVarFloat: {SizeInBytes = 4u;}break;
    default:        {SizeInBytes = 0u;}break;
    }
}

qVariable::qVariable(const qVariable &ref):QObject(nullptr)
{
    Name = ref.Name;
    Type = ref.Type;
    Address = ref.Address;
    Gain = ref.Gain;
    Offset  = ref.Offset;
    SizeInBytes = ref.SizeInBytes;
}

void qVariable::setValue(double value)
{
    var_t  val;
    size_t size = 0;

    switch (Type)
    {
    case qVarUint8:
    {
        size = 1;
        val.ui8Data = (value/Gain) + Offset;
    }
        break;
    case qVarSint8:
    {
        size = 1;
        val.si8Data = (value/Gain) + Offset;
    }
        break;
    case qVarUint16:
    {
        size = 2;
        val.ui16Data = (value/Gain) + Offset;
    }
        break;
    case qVarSint16:
    {
        size = 2;
        val.si16Data = (value/Gain) + Offset;
    }
        break;
    case qVarUint32:
    {
        size = 4;
        val.ui32Data = (value/Gain) + Offset;
    }
        break;
    case qVarSint32:
    {
        size = 4;
        val.si32Data = (value/Gain) + Offset;
    }
        break;

    case qVarFloat:
    {

        val.flData = (value/Gain) + Offset;
        size = 4;
    }
        break;

    default:
    {
        size =0;
        val.ui32Data = 0;
    }
        break;
    }

    if(size > 0)
    {
        emit valueChanged(Address,val.Buffer,size);
    }
}

double qVariable::getValue()
{
    if(ReadValues.length() > 0)
    {
        return ReadValues.last();
    }

    //emit Log("WARNING : No values are available.");

    return 0;
}
void qVariable::triggerDisplayData()
{
    emit displayData(Name,TStampValues,ReadValues);
}
uint32_t qVariable::updateRawData(uint64_t timestamp,uint64_t address,uint8_t *data,uint32_t size)
{

    double val=0;
    uint32_t Transfersize = 0;

    uint64_t obj_Addr_Cnt=0;
    uint64_t last_found_obj_Addr_Cnt=Address;
    uint64_t buffer_Addr_Cnt=0;
    uint8_t  data_updated=0;

    if(Type != qVarUnknown)
    {

        for(buffer_Addr_Cnt=address;buffer_Addr_Cnt<(address+size);buffer_Addr_Cnt++)
        {
            for(obj_Addr_Cnt=last_found_obj_Addr_Cnt;obj_Addr_Cnt<(Address+SizeInBytes);obj_Addr_Cnt++)
            {
                if(buffer_Addr_Cnt == obj_Addr_Cnt)
                {
                    /* ---- Transfer the byte -----*/
                    RxData.Buffer[obj_Addr_Cnt-Address]=data[buffer_Addr_Cnt-address];
                    /* ---- Take advantage that the addresses are ordered ----*/
                    last_found_obj_Addr_Cnt = obj_Addr_Cnt;
                    /* ---- The regions have at least 1 byte in comon ----*/
                    data_updated=1;
                    break;
                }
            }
        }


        // ---- Check if the data was updated  ----
        if(data_updated !=0)
        {
            // ---- Assume Intel endianess ----
            switch (Type)
            {
            case qVarUint8:
            {
                val = (double)RxData.ui8Data;
            }
                break;
            case qVarSint8:
            {
                val = (double)RxData.si8Data;
            }
                break;
            case qVarUint16:
            {
                val = (double)RxData.ui16Data;
            }
                break;
            case qVarSint16:
            {
                val = (double)RxData.si16Data;
            }
                break;
            case qVarUint32:
            {
                val = (double)RxData.ui32Data;
            }
                break;
            case qVarSint32:
            {
                val = (double)RxData.si32Data;
            }
                break;

            case qVarFloat:
            {
                val = (double)RxData.flData;
            }
                break;

            default:
            {
                val = 0;
            }
                break;
            }

            // ---- Append to the value list -----
            ReadValues.append((val - Offset)*Gain);
            TStampValues.append(timestamp*0.001);

            while(ReadValues.size() > ReceiveMaxBufferSize)
            {
                ReadValues.removeFirst();
            }

            while(TStampValues.size() > ReceiveMaxBufferSize)
            {
                TStampValues.removeFirst();
            }
            emit Log(QString("DEBUG :[%1]").arg(timestamp)+Name+"="+QString("%1").arg(val));
            emit displayData(Name,TStampValues,ReadValues);
        }

    }

    return Transfersize;
}

#define DBG_ADDRESS_IDENT "INFO : Address :"
#define DBG_TYPE_IDENT    "INFO : Type    :"
#define DBG_SIZE_IDENT    "INFO : Size    :"
#define DBG_ERROR_IDENT   "ERROR :"
#define DBG_DEBUG_IDENT   "DEBUG :"

void qVariable::updateElfInformation(QString &elf)
{
    QProcess readelf;
    int size=0;
    QString StrType;
    QStringList param ;
    param.append(elf);
    param.append(Name);
    readelf.start("SymbolDecoder", param);
    emit Log("INFO : Call : SymbolDecoder "+elf+" "+Name);

    if (!readelf.waitForFinished())
    {
        emit Log("ERROR : Error starting the Symbol Decoder");
        return ;
    }
    while(readelf.canReadLine())
    {
        QString line = readelf.readLine();
        if(line.contains(DBG_ADDRESS_IDENT))
        {
            bool ok=false;
            QList<QString>substrs = line.split(":");
            Address = substrs[2].toLong(&ok,16);
            emit Log(line.split("\n").first());
        }else
            if(line.contains(DBG_SIZE_IDENT))
            {
                bool ok=false;
                QList<QString>substrs = line.split(":");
                size = substrs[2].toLong(&ok,10);
                emit Log(line.split("\n").first());
            }else
                if(line.contains(DBG_TYPE_IDENT))
                {
                    QList<QString>substrs = line.split(":");
                    StrType = substrs[2];
                    emit Log(line.split("\n").first());
                }else
                    if(line.contains("INFO :"))
                    {
                        emit Log(line);
                    }
    }


    SizeInBytes = size;
    // ---- First set the type to unknown ----
    Type = qVarUnknown;
    if((StrType.contains("DW_ATE_address")) ||
            (StrType.contains("DW_ATE_boolean")) ||
            (StrType.contains("DW_ATE_unsigned"))||
            (StrType.contains("DW_ATE_unsigned_char"))||
            (StrType.contains("DW_ATE_unsigned_fixed"))||
            (StrType.contains("_ENUM_")))
    {
        switch(size)
        {
        case sizeof(uint8_t) :{Type = qVarUint8;}break;
        case sizeof(uint16_t):{Type = qVarUint16;}break;
        case sizeof(uint32_t):{Type = qVarUint32;}break;
        }
    }
    if((StrType.contains("DW_ATE_signed"))||
            (StrType.contains("DW_ATE_signed_char"))||
            (StrType.contains("DW_ATE_signed_fixed")))
    {
        switch(size)
        {
        case sizeof(uint8_t) :{Type = qVarSint8;}break;
        case sizeof(uint16_t):{Type = qVarSint16;}break;
        case sizeof(uint32_t):{Type = qVarSint32;}break;
        }
    }
    if((StrType.contains("DW_ATE_float")))
    {
        switch(size)
        {
        case sizeof(_Float32) :{Type = qVarFloat;}break;
        }
    }
}

void qVariable::requestData()
{
    emit requestRawData(Address,SizeInBytes);
}
