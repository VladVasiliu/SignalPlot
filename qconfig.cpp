/*Copyright (C) 2019  Vlad Vasiliu

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/
#include "qconfig.h"

qConfig::qConfig(QObject *parent) : QObject(parent)
{

}
void qConfig::writeElfPath(QString &filename)
{
    root["elf"]=filename;
}
void qConfig::writeVariable(qVariable &var)
{
    QJsonObject var_obj,obj;
    var_obj["type"] = var.Type;
    var_obj["address"]=(int)var.Address;
    var_obj["gain"]=var.Gain;
    var_obj["offset"]=var.Offset;
    varlist[var.Name]=var_obj;
}

void qConfig::writeConfig(QString filename)
{
    // ---- Complete teh JSON document ----
    root["variables"]=varlist;
    doc.setObject(root);
    // ----- Save as a string ----
    QString json_string = doc.toJson();


    QFile save_file(filename);

    if(!save_file.open(QIODevice::WriteOnly))
    {
        emit Log("ERROR : Error during saving : "+filename);
        return;
    }
    save_file.write(json_string.toLocal8Bit());
    save_file.close();
}
void qConfig::readConfig(QString filename)
{
    QFile file(filename);
    QString json_string;
    if(!file.open(QIODevice::ReadOnly))
    {
        emit Log("ERROR : Error during Opening : "+filename);
        return;
    }
    json_string = file.readAll();
    file.close();
    auto json_doc=QJsonDocument::fromJson(json_string.toLocal8Bit());
    json_doc.fromJson(json_string.toLocal8Bit());
    if(json_doc.isNull())
    {
        emit Log("ERROR : Error during reading JSON data from :"+filename+json_string);
        return;
    }

    QJsonObject json_obj=json_doc.object();

    if(json_obj.isEmpty())
    {
        emit Log("ERROR : Error during reading JSON data(Empty)");
        return;
    }

    QVariantMap json_map = json_obj.toVariantMap();

    // ---- Set Elf File -----
    emit SetElf(json_map["elf"].toString());
    emit Log("INFO : Elf File : " +json_map["elf"].toString());


    QJsonObject varlist = json_map["variables"].toJsonObject();
    foreach(auto key,varlist.keys())
    {
        QJsonObject varobj = varlist[key].toObject();
        emit Log("INFO : Set Variable:"+key);
        emit SetVariable(key,varobj["type"].toInt(),varobj["address"].toInt(),varobj["gain"].toDouble(),varobj["offset"].toDouble());
    }
}
