/*Copyright (C) 2019  Vlad Vasiliu

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/
#ifndef QINTERPRETER_H
#define QINTERPRETER_H

#include <QObject>

class qInterpreter : public QObject
{
    Q_OBJECT
public:
    explicit qInterpreter(QObject *parent = nullptr);

public slots:
    void Decode(QString cmd);


signals:
    double GetValue(QString variable);
    void   SetValue(QString Varaible,double val);
    void   Log(QString text);

};

#endif // QINTERPRETER_H
