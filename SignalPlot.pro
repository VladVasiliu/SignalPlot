QT       += core gui printsupport network serialport

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    SignalPlotWindow.cpp \
    cansoket.cpp \
    main.cpp \
    qcandaq.cpp \
    qconfig.cpp \
    qcustomplot.cpp \
    qdebugsim.cpp \
    qinterpreter.cpp \
    qopenocd.cpp \
    qosc.cpp \
    qrouter.cpp \
    qupdi.cpp \
    qvariable.cpp \
    semaphore.cpp

HEADERS += \
    SignalPlotWindow.h \
    cansoket.h \
    qcandaq.h \
    qconfig.h \
    qcustomplot.h \
    qdebugsim.h \
    qinterpreter.h \
    qopenocd.h \
    qosc.h \
    qrouter.h \
    qupdi.h \
    qvariable.h \
    semaphore.h

FORMS += \
    SignalPlotWindow.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target
