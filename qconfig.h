/*Copyright (C) 2019  Vlad Vasiliu

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/
#ifndef QCONFIG_H
#define QCONFIG_H

#include <QObject>
#include <QFile>
#include <QJsonDocument>
#include <QJsonValue>
#include <QJsonArray>
#include <QJsonObject>

#include "qvariable.h"
class qConfig : public QObject
{
    Q_OBJECT
    QJsonDocument doc;
    QJsonObject root;
    QJsonObject varlist;
public:
    explicit qConfig(QObject *parent = nullptr);

public slots:
    void readConfig(QString filename);
    void writeConfig(QString filename);
    void writeVariable(qVariable &var);
    void writeElfPath(QString &filename);
signals:
    void Log(QString data);
    void SetElf(QString data);
    void SetVariable(QString name,int type,uint64_t address,double gain,double offset);
};

#endif // QCONFIG_H
