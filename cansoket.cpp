#include "cansoket.h"
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <signal.h>
#include <libgen.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <sys/uio.h>
#include <linux/can.h>
#include <net/if.h>
#include <linux/can/raw.h>




cansoket::cansoket()
{
}




void cansoket::canInit(const char *name)
{
    struct ifreq ifr;
    struct sockaddr_can addr;

    /* ---- Open a can socket ----*/
    if ((skt = socket(PF_CAN, SOCK_RAW, CAN_RAW)) > 0)
    {
        addr.can_family = PF_CAN;
        strcpy(ifr.ifr_name, name);
        ioctl(skt, SIOCGIFINDEX, &ifr);
        addr.can_ifindex = ifr.ifr_ifindex;

        /* ---- Open a can socket ----*/
        if (bind(skt, (struct sockaddr *)&addr, sizeof(addr)) < 0)
        {
            close(skt);
            skt = -1;
            LastError = "ERROR :  Error during Bind ";

        }
        else
        {

            SktNotifier = new QSocketNotifier( skt, QSocketNotifier::Read);
            QObject::connect(SktNotifier,&QSocketNotifier::activated,this, &cansoket::dataReceived);
            LastError = "INFO : Connect suceeded ";

        }

    }
    else
    {
        LastError = "ERROR :  Error durring opening the socket ";
        skt = -1;
    }
}
int cansoket::canTx(int ID, int DLC, unsigned char *data)
{
    struct can_frame frame;
    int ret = 0;

    if(skt <0 ) return -1;

    /* ---- Clear working data ----*/
    memset(&frame,0,sizeof(frame));
    frame.can_id  = ID;
    frame.can_dlc = DLC;
    memcpy(frame.data,data,DLC);

    ret = write(skt, &frame, sizeof(frame));

    if(ret < 0 )
    {
        LastError = "ERROR :  Error sending the frame";
    }
    else
    {
        LastError = "";
    }

    return ret;

}

int cansoket::canRx(int &MAXDLC, unsigned char *data)
{

    struct can_frame frame;

    if(skt < 0 ) return -1;

    /* ---- Clear working data ----*/
    memset(&frame,0,sizeof(frame));

    /* ---- Read the data ----- */
    if ( read(skt, &frame, sizeof(frame)) < 0)
    {
        LastError = "ERROR :  Error receiving the frame";
    }
    else
    {
        if(frame.can_dlc <= MAXDLC)
        {
            memcpy(data,frame.data,frame.can_dlc);
            LastError = "";
            MAXDLC = frame.can_dlc;
        }
        else
        {
            LastError = "ERROR :  Error receiving the frame (Buffer insufficient !)";
        }
    }

    return frame.can_id;
}

void cansoket::dataReceived()
{
    emit dataAvailable();
}
bool cansoket::canIsOpened(void)
{
    return (skt >= 0)? true:false;
}

void cansoket::canClose(void)
{
    if(skt >= 0)
    {
        close(skt);
        skt=-1;
        LastError = "";
    }
}

cansoket::~cansoket()
{
    canClose();
}

void cansoket::canSetLoopback(bool loopback)
{
    int loopbck = (loopback != false)? 1:0; /* 0 = disabled, 1 = enabled (default) */
    setsockopt(skt, SOL_CAN_RAW, CAN_RAW_LOOPBACK, &loopbck, sizeof(loopbck));
}
