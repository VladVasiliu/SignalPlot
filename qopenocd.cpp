/*Copyright (C) 2019  Vlad Vasiliu

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/
#include "qopenocd.h"
#include <QDateTime>

QOpenOCD::QOpenOCD(QObject *parent) : QObject(parent)
{

}
QOpenOCD::~QOpenOCD()
{
    Disconnect();
}
void QOpenOCD::Connect(int port)
{
    Socket.connectToHost("localhost",port);

    if(!Socket.waitForConnected(3000))
    {
        emit Log("ERROR : Connection Timeout!(OpenOCD)");
    }
    else
    {
        Rxbytes.clear();
        emit Log("INFO : Connection Performed!(OpenOCD)");
        connect(&Socket,&QTcpSocket::readyRead,this,&QOpenOCD::ReadFromSocket);
        timestamp_start = QDateTime::currentMSecsSinceEpoch();
        emit resetData();
    }
}
void QOpenOCD::Disconnect()
{
    if(Socket.isOpen())
    {
        Socket.close();
        emit Log("INFO : Disconnected!(OpenOCD)");
        Rxbytes.clear();
    }
}
void QOpenOCD::WriteReq8(uint64_t address,uint8_t data)
{
    if(Socket.isWritable()&&(Socket.state() == QTcpSocket::ConnectedState))
    {
        Socket.write(QString("mwb"+QString(" 0x%1").arg(address,0,16)+QString(" 0x%1").arg(data,0,16)+"\r\n").toUtf8());

        if(Socket.waitForBytesWritten(500)==false)
        {
            emit Log("ERROR :(OpenOCD) Communication timeout durring writing ! (Address "+QString("0x%1").arg(address,0,16)+")");
        }

    }
}
void QOpenOCD::WriteReq16(uint64_t address,uint16_t data)
{
    if(Socket.isWritable()&&(Socket.state() == QTcpSocket::ConnectedState))
    {
        Socket.write(QString("mwh"+QString(" 0x%1").arg(address,0,16)+QString(" 0x%1").arg(data,0,16)+"\r\n").toUtf8());

        if(Socket.waitForBytesWritten(500)==false)
        {
            emit Log("ERROR :(OpenOCD) Communication timeout durring writing ! (Address "+QString("0x%1").arg(address,0,16)+")");
        }
    }
}
void QOpenOCD::WriteReq32(uint64_t address,uint32_t data)
{
    if(Socket.isWritable()&&(Socket.state() == QTcpSocket::ConnectedState))
    {
        Socket.write(QString("mww"+QString(" 0x%1").arg(address,0,16)+QString(" 0x%1").arg(data,0,16)+"\r\n").toUtf8());

        if(Socket.waitForBytesWritten(500)==false)
        {
            emit Log("ERROR :(OpenOCD) Communication timeout durring writing ! (Address "+QString("0x%1").arg(address,0,16)+")");
        }
    }
}
void QOpenOCD::ReadReq8(uint64_t address)
{
    if(Socket.isWritable()&&(Socket.state() == QTcpSocket::ConnectedState))
    {
        Socket.write(QString("mrb"+QString(" 0x%1").arg(address,0,16)+"\r\n").toUtf8());

        if(Socket.waitForBytesWritten(500)==false)
        {
            emit Log("ERROR :(OpenOCD) Communication timeout durring reading ! (Address "+QString("0x%1").arg(address,0,16)+")");
        }
    }
}
void QOpenOCD::ReadReq16(uint64_t address)
{
    if(Socket.isWritable()&&(Socket.state() == QTcpSocket::ConnectedState))
    {
        Socket.write(QString("mrh"+QString(" 0x%1").arg(address,0,16)+"\r\n").toUtf8());

        if(Socket.waitForBytesWritten(500)==false)
        {
            emit Log("ERROR :(OpenOCD) Communication timeout durring reading ! (Address "+QString("0x%1").arg(address,0,16)+")");
        }
    }
}void QOpenOCD::ReadReq32(uint64_t address)
{
    if(Socket.isWritable()&&(Socket.state() == QTcpSocket::ConnectedState))
    {
        Socket.write(QString("mrw"+QString(" 0x%1").arg(address,0,16)+"\r\n").toUtf8());

        if(Socket.waitForBytesWritten(500)==false)
        {
            emit Log("ERROR :(OpenOCD) Communication timeout durring reading ! (Address "+QString("0x%1").arg(address,0,16)+")");
        }
    }
}

void QOpenOCD::ReadFromSocket()
{
    if(!Socket.isReadable()){return;}
    QString command;
    QStringList command_parts;

    while ((Socket.bytesAvailable()) || (Rxbytes.contains('>')))
    {
        Rxbytes.remove(0,Rxbytes.indexOf('>')+1);
        Rxbytes.append(Socket.readAll());
        Rxbytes.replace('\0',' ');
        Rxbytes.replace('\n',' ');
        Rxbytes.replace('\r',' ');

        if(Rxbytes.contains('>'))
        {

            emit Log(QString("DEBUG : Data Rx(buffer len) : %1").arg(Rxbytes.size()));
            command=Rxbytes;

            command_parts=command.split(' ');

            if(command_parts.count() >= 6)
            {
                //emit Log("DEBUG :(OpenOCD) Data Rx(parts) : "+command_parts[1]+"|"+command_parts[2]+"|"+command_parts[5]);

                bool ok1=false;
                bool ok2=false;

                uint64_t addr = command_parts[2].toULong(&ok1,16);
                uint32_t data = command_parts[5].toULong(&ok2,10);
                uint64_t timestamp = QDateTime::currentMSecsSinceEpoch() - timestamp_start;

                if(ok1&&ok2)
                {
                    int size=1;
                    if(command_parts[1]=="mrh"){size=2;}
                    if(command_parts[1]=="mrw"){size=4;}
                    emit Log("DEBUG :(OpenOCD) OpenOCD Read :"+QString("[0x%1]").arg(addr,0,16)+QString("[%1]").arg(size)+QString(" = 0x%1").arg(data,0,16));
                    emit updateRawData(timestamp,addr,(uint8_t*)&data,size);
                }
            }
        }
    }

}
void QOpenOCD::ReadReq(uint64_t address,uint32_t size)
{
    if(Socket.isOpen())
    {
        if((size%4)==0)
        {
            for(uint32_t datacnt = 0;datacnt<size;datacnt+=4)
            {
                ReadReq32(address+datacnt);
            }
        }
        else if((size % 2)==0)
        {
            for(uint32_t datacnt = 0;datacnt<size;datacnt+=2)
            {
                ReadReq16(address+datacnt);
            }
        }
        else
        {
            for(uint32_t datacnt = 0;datacnt<size;datacnt+=1)
            {
                ReadReq8(address+datacnt);
            }
        }
    }
}
void QOpenOCD::Write(uint64_t address,uint8_t *data,uint32_t size)
{
    if(Socket.isOpen())
    {
        if((size%4)==0)
        {
            for(uint32_t datacnt = 0;datacnt<size;datacnt+=4)
            {
                WriteReq32(address+datacnt,((uint32_t *)data)[datacnt]);
            }
        }
        else if((size % 2)==0)
        {
            for(uint32_t datacnt = 0;datacnt<size;datacnt+=2)
            {
                WriteReq16(address+datacnt,((uint16_t *)data)[datacnt]);
            }
        }
        else
        {
            for(uint32_t datacnt = 0;datacnt<size;datacnt+=1)
            {
                WriteReq8(address+datacnt,((uint8_t *)data)[datacnt]);
            }
        }
    }
    else
    {
        emit Log("ERROR :(OpenOCD) Writing is not possible since no connection is active");
    }
}
