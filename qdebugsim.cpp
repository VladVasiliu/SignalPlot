#include "qdebugsim.h"

#include <QDateTime>

QDebugSim::QDebugSim(QObject *parent) : QObject(parent)
{

}
QDebugSim::~QDebugSim( )
{
    Disconnect();
}

void QDebugSim::Connect(int port)
{
    emit Log(QString("INFO : connect to localhost port %1").arg(port));
    Socket.connectToHost("localhost",port);

    if(!Socket.waitForConnected(3000))
    {
        emit Log("ERROR : Connection Timeout!(Debug Sim)");
    }
    else
    {
        Rxbytes.clear();
        emit Log("INFO : Connection Performed!(Debug Sim)");
        connect(&Socket,&QTcpSocket::readyRead,this,&QDebugSim::ReadFromSocket);
        timestamp_start = QDateTime::currentMSecsSinceEpoch();
        emit resetData();
    }
}
void QDebugSim::Disconnect()
{
    if(Socket.isOpen())
    {
        Socket.close();
        emit Log("INFO :(Debug Sim) Disconnected!");
        Rxbytes.clear();
    }
}

void QDebugSim::ReadFromSocket()
{

    QString command;

    command = Socket.readLine();

    command.replace('[',' ');
    command.replace(']',' ');
    command.replace('=',' ');

     QStringList command_parts = command.split(" ");

     if(command_parts.count() >= 7)
     {
         emit Log("DEBUG :(OpenOCD) Data Rx(parts) : "+command_parts[0]+"|"+command_parts[1]+"|"+command_parts[2]);

         bool ok0=false;
         bool ok1=false;
         bool ok2=false;
         bool ok3=false;

         uint64_t timestamp =  command_parts[1].toULong(&ok0,16);
         uint64_t addr      =  command_parts[3].toULong(&ok1,16);
         uint64_t size      =  command_parts[5].toULong(&ok2,16);
         uint32_t data      =  command_parts[7].toULong(&ok3,16);



         if(ok0&&ok1&&ok2&&ok3)
         {
             emit Log("DEBUG :(Simread) Read :"+QString("[0x%1]").arg(addr,0,16)+QString("[%1]").arg(size)+QString(" = 0x%1").arg(data,0,16));
             emit updateRawData(timestamp,addr,(uint8_t*)&data,size);
         }
     }

}
void QDebugSim::ReadReq(uint64_t address,uint32_t size)
{
    if(Socket.isOpen())
    {
        if(Socket.isWritable()&&(Socket.state() == QTcpSocket::ConnectedState))
        {
            Socket.write(QString(QString("[0x%1]").arg(address,0,16)+QString(QString("[0x%1]\n").arg(size,0,16))).toUtf8());

            if(Socket.waitForBytesWritten(500)==false)
            {
                emit Log("ERROR :(Debug Sim) Communication timeout durring reading ! (Address "+QString("0x%1").arg(address,0,16)+")");
            }
        }
    }
}
void QDebugSim::Write(uint64_t address,uint8_t *data,uint32_t size)
{
    union
    {
     uint8_t buff[8];
     uint8_t ui8val;
     uint16_t ui16val;
     uint32_t ui32val;
     uint64_t ui64val;

    } dat;

    dat.ui64val =0;

    if(size > sizeof(dat)) return;

    memcpy(dat.buff,data,size);

    if(Socket.isOpen())
    {
        switch (size)
        {
        case 1: {Socket.write(QString(QString("[0x%1]").arg(address,0,16)+QString(QString("[0x%1]").arg(size,0,16)+QString(QString("=0x%1\n").arg(dat.ui8val,0,16)))).toUtf8());}break;
        case 2: {Socket.write(QString(QString("[0x%1]").arg(address,0,16)+QString(QString("[0x%1]").arg(size,0,16)+QString(QString("=0x%1\n").arg(dat.ui16val,0,16)))).toUtf8());}break;
        case 4: {Socket.write(QString(QString("[0x%1]").arg(address,0,16)+QString(QString("[0x%1]").arg(size,0,16)+QString(QString("=0x%1\n").arg(dat.ui32val,0,16)))).toUtf8());}break;
        case 8: {Socket.write(QString(QString("[0x%1]").arg(address,0,16)+QString(QString("[0x%1]").arg(size,0,16)+QString(QString("=0x%1\n").arg(dat.ui64val,0,16)))).toUtf8());}break;
        default:
        {
            emit Log("ERROR :(Debug Sim) Invalid writing size");
            return;
        }break;
        }



        if(Socket.waitForBytesWritten(500)==false)
        {
            emit Log("ERROR :(Debug Sim) Communication timeout durring reading ! (Address "+QString("0x%1").arg(address,0,16)+")");
        }
    }
    else
    {
        emit Log("ERROR :(Debug Sim) Writing is not possible since no connection is active");
    }
}
