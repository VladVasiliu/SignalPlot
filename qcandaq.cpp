#include "qcandaq.h"
#include <QDateTime>
#include <QTime>
qcandaq::qcandaq(QObject *parent)
    : QObject{parent}
{
}

void qcandaq::Connect(QString port)
{
    if(sock.canIsOpened() == false)
    {
        QStringList parameters;

        sema.acquire();
        requests.clear();
        sema.release();

        parameters = port.split(" ",Qt::SkipEmptyParts);

        emit Log("INFO : Can port " + parameters[0] + " was opened !");

        CanSocketName =parameters[0];

        sock.canInit(CanSocketName.toStdString().c_str());

        if(parameters.size() > 1)
        {
            TxIDReadReq  = parameters[1].toShort();
        }

        if(parameters.size() > 2)
        {
            TxIDWriteReq = parameters[2].toShort();
        }

        if(parameters.size() > 3)
        {
            RxID         = parameters[3].toShort();
        }

        emit Log("INFO : Mem Read     Can Id = " + QString("%1").arg(TxIDReadReq));
        emit Log("INFO : Mem Write    Can Id = " + QString("%1").arg(TxIDWriteReq));
        emit Log("INFO : Mem Response Can Id = " + QString("%1").arg(RxID));

        emit Log(sock.LastError);


        connect(&sock,&cansoket::dataAvailable,this,&qcandaq::readyRead);
        connect(&RxTimeoutTimer,&QTimer::timeout,this,&qcandaq::ReqTimeout);


        RxTimeoutTimer.setInterval(1000);
        RxTimeoutTimer.start();

        emit resetData();
        timestamp_start = QDateTime::currentMSecsSinceEpoch();
    }
    else
    {

        emit Log("INFO : Can port " + port + " was already opened !");
    }
}

void qcandaq::Disconnect()
{
    if(sock.canIsOpened() != false)
    {
        disconnect(&sock,&cansoket::dataAvailable,this,&qcandaq::readyRead);
        disconnect(&RxTimeoutTimer,&QTimer::timeout,this,&qcandaq::ReqTimeout);
        sock.canClose();
        requests.clear();
    }
}
void qcandaq::AddRequest(uint8_t ID,uint8_t dlc,uint8_t raw[8])
{
    qcanrequest req ;

    sema.acquire();

    if(requests.size() >= MaxReqBufferLen)
    {
        emit Log ("ERROR : Buuffer Overflow "+QString("%1").arg(requests.size())+" were dropped");
        requests.clear();
    }
    else
    {

        if(requests.size() == 0)
        {
            sock.canTx(ID,dlc,raw);
        }

        req.ID = ID;
        req.dlc = dlc;
        req.data[0] = raw[0];
        req.data[1] = raw[1];
        req.data[2] = raw[2];
        req.data[3] = raw[3];
        req.data[4] = raw[4];
        req.data[5] = raw[5];
        req.data[6] = raw[6];
        req.data[7] = raw[7];

        requests.push_back(req);
    }

    sema.release();

}

void qcandaq::Write(uint64_t address, uint8_t *data, uint32_t size)
{
    uint8_t raw[8] = {0,0,0,0,0,0,0,0};
    uint8_t len = 4;
    raw[0] = (address& 0xFF000000ul) >>24;
    raw[1] = (address& 0x00FF0000ul) >>16;
    raw[2] = (address& 0x0000FF00ul) >>8;
    raw[3] = (address& 0x000000FFul) >>0;

    switch(size)
    {
    case 1:{raw[4] = data[0]; len+=1;}break;
    case 2:{raw[4] = data[0]; raw[5] = data[1];len+=2;}break;
    case 4:{raw[4] = data[0]; raw[5] = data[1];raw[6] = data[2]; raw[7] = data[3];len+=4;}break;
    default:
    {
        emit Log ("ERROR : CAN Write Mem : Unexpected transfer size ! "+QString("(%1)").arg(size));
    }break;
    }

    if(len > 4)
    {
        AddRequest(TxIDWriteReq,len,raw);
    }else
    {
        emit Log ("ERROR : CAN Write Mem : Unexpected CAN size ! "+QString("(%1)").arg(len));
    }
}


void qcandaq::ReadReq(uint64_t address, uint32_t size)
{
    uint8_t raw[8] = {0,0,0,0,0,0,0,0};
    uint8_t len = 5;
    raw[0] = (address& 0xFF000000ul) >>24;
    raw[1] = (address& 0x00FF0000ul) >>16;
    raw[2] = (address& 0x0000FF00ul) >>8;
    raw[3] = (address& 0x000000FFul) >>0;
    raw[4] = (size   & 0x00000007ul) >>0;

    AddRequest(TxIDReadReq,len,raw);
}

void qcandaq::readyRead()
{
    uint64_t timestamp = 0;
    uint64_t address = 0;
    uint8_t raw[8] = {0,0,0,0,0,0,0,0};
    uint8_t data[4] = {0,0,0,0};
    int     len = 8;
    uint8_t size = 0;
    int     ID = sock.canRx(len,raw);

    if(ID != RxID)
    {
        return;
    }


    if(len < 5)
    {
        emit Log("ERROR : Unexpected Rx length "+QString("(%1)").arg(len));
    }
    else
    {
        address  = ((uint64_t)raw[0])<<24;
        address |= ((uint64_t)raw[1])<<16;
        address |= ((uint64_t)raw[2])<<8;
        address |= ((uint64_t)raw[3])<<0;

        switch(len)
        {
        case 5:{data[0] = raw[4]; size=1;}break;
        case 6:{data[0] = raw[4]; data[1] = raw[5];size=2;}break;
        case 8:{data[0] = raw[4]; data[1] = raw[5];data[2] = raw[6];data[3] = raw[7];size=4;}break;
        default:{}break;
        }
        timestamp = QDateTime::currentMSecsSinceEpoch() - timestamp_start;
        emit  updateRawData(timestamp,address,data,size);
        RxTimeoutTimer.stop();
        RxTimeoutTimer.start();

        sema.acquire();
        if(requests.size() > 0)
        {
          requests.pop_front();
        }

        if(requests.size() > 0)
        {
            sock.canTx(requests.front().ID,requests.front().dlc,requests.front().data);
        }

        sema.release();

    }



}

void qcandaq::ReqTimeout()
{
    sema.acquire();
    if(requests.size() >0)
    {
        emit Log("ERROR :  Rx Timeout !");
        sock.canTx(requests.front().ID,requests.front().dlc,requests.front().data);
        RxTimeoutTimer.setInterval(1000);
        RxTimeoutTimer.start();
    }
    sema.release();

}


