/*Copyright (C) 2019  Vlad Vasiliu

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/
#ifndef QOPENOCD_H
#define QOPENOCD_H

#include <QObject>
#include <QTcpSocket>

class QOpenOCD : public QObject
{
    Q_OBJECT

    QTcpSocket Socket;
    QByteArray Rxbytes;
    double timestamp_start;
public:
    explicit QOpenOCD(QObject *parent = nullptr);
    ~QOpenOCD();

    void WriteReq32(uint64_t address,uint32_t data);
    void WriteReq16(uint64_t address,uint16_t data);
    void WriteReq8(uint64_t address,uint8_t data);
    void ReadReq32(uint64_t address);
    void ReadReq16(uint64_t address);
    void ReadReq8(uint64_t address);

public slots:
    void Connect(int port);
    void Disconnect();
    void Write(uint64_t address,uint8_t *data,uint32_t size);
    void ReadReq(uint64_t address,uint32_t size);
    void ReadFromSocket();

signals:
    void resetData();
     uint32_t updateRawData(uint64_t timestamp,uint64_t address,uint8_t *data,uint32_t size);
     void Log(QString text);
};

#endif // QOPENOCD_H
