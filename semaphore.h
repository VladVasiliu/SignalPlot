#ifndef SEMAPHORE_H
#define SEMAPHORE_H



#include <mutex>
#include <condition_variable>

class semaphore {
    std::mutex mutex_;
    std::condition_variable condition;
    unsigned long count = 0;

public:
    semaphore();
    void release();
    void acquire();
    bool try_acquire();
};

#endif // SEMAPHORE_H
