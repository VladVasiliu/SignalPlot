/*Copyright (C) 2019  Vlad Vasiliu

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/
#ifndef QVARIABLE_H
#define QVARIABLE_H

#include <QObject>
#include <QDebug>
enum qVariableType
{
    qVarUnknown,
    qVarUint8,
    qVarSint8,
    qVarUint16,
    qVarSint16,
    qVarUint32,
    qVarSint32,
    qVarFloat
};
union var_t
{
    uint8_t  Buffer[sizeof(uint64_t)];
    uint8_t  ui8Data;
    int8_t   si8Data;
    uint16_t ui16Data;
    int16_t  si16Data;
    uint32_t ui32Data;
    int32_t  si32Data;
    uint64_t ui64Data;
    int64_t  si64Data;
    float    flData;
    double   dflData;
};


class qVariable : public QObject
{
    Q_OBJECT

    var_t RxData;

public:
    QList<double> ReadValues;
    QList<double> TStampValues;


    int  ReceiveMaxBufferSize=1024;
    QString Name;
    uint8_t SizeInBytes;
    uint64_t Address=0;
    qVariableType Type=qVarUnknown;

    double WriteValue=0;
    double Gain=0;
    double Offset=0;

    //---- Constructors / Destructors -----
    explicit qVariable(QObject *parent = nullptr);
    qVariable(QString &name,uint64_t address,qVariableType type,double gain,double offset);
    qVariable(const qVariable &ref);
    ~qVariable(){}
    // ---- Necessary operators for a list ------
    qVariable operator = (const qVariable &ref){Name = ref.Name;Type = ref.Type;Address = ref.Address;Gain = ref.Gain;Offset = ref.Offset;return ref;}



    // ---- Set a Variable----
    void setValue(double value);
    double getValue();


public slots:
    void requestData();
    void updateElfInformation(QString &elf);
    uint32_t updateRawData(uint64_t timestamp,uint64_t address,uint8_t *data,uint32_t size);
    void triggerDisplayData();
    void resetBuffers(){ReadValues.clear();TStampValues.clear();}
signals:
    void Log(QString text);
    void valueChanged(uint64_t address,uint8_t *data,uint32_t size);
    void requestRawData(uint64_t address, uint32_t size);
    void displayData(QString name,QList<double> &timestamps_ms,QList<double> &data);
};

#endif // QVARIABLE_H
