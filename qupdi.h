#ifndef QUPDI_H
#define QUPDI_H

#include <QObject>
#include <QSerialPort>
#define UDPI_LDS_OPCODE 0x04
#define UDPI_STS_OPCODE 0x44
#define UDPI_SYNC       0x55
#define UDPI_ACK        0x40
#define UDPI_BREAK      0x00



class qUPDI :public QObject
{
     Q_OBJECT

    enum UART_Operation
    {
        UART_Unknown=0,
        UART_Tx,
        UART_Rx,
    };

    QByteArray TxRxExpectedData;
    QByteArray TxRxOperation;

    QSerialPort serial;
    QByteArray RxData;
    double timestamp_start;

    void ResetComm();
    void ConfigureComm();
    void UARTTxRequest(QByteArray &data);
    void UARTRxRequest();
    void ErrorHandler();

public:
    explicit qUPDI(QObject *parent = nullptr);
    ~qUPDI();

    void WriteReq8(uint64_t address,uint8_t val);
    void ReadReq8(uint64_t address);


public slots:
    void Connect(QString port);
    void Disconnect();
    void Write(uint64_t address,uint8_t *data,uint32_t size);
    void ReadReq(uint64_t address,uint32_t size);
    void readyRead();


signals:
    void resetData();
    uint32_t updateRawData(uint64_t timestamp,uint64_t address,uint8_t *data,uint32_t size);
    void Log(QString text);
};

#endif // QUPDI_H
