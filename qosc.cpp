/*Copyright (C) 2019  Vlad Vasiliu

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/
#include "qosc.h"

void qOsc::setTracerPropieties()
{
    Tracer1->setStyle(QCPItemTracer::tsCrosshair);
    Tracer1->setPen(QPen(Qt::cyan));
    Tracer1->setBrush(Qt::cyan);
    Tracer1->setSize(10);
    Tracer1->setVisible(false);
    Tracer1->setInterpolating(false);
    Tracer2->setStyle(QCPItemTracer::tsCrosshair);
    Tracer2->setPen(QPen(Qt::magenta));
    Tracer2->setBrush(Qt::magenta);
    Tracer2->setSize(10);
    Tracer2->setVisible(false);
    Tracer2->setInterpolating(false);

}
void qOsc::setCursorInfoPropieties()
{
    QFont font;
    font.setFamily("Monospace");
    font.setPointSize(8);
    CursorInfo->setPositionAlignment(Qt::AlignLeft | Qt::AlignTop);
    CursorInfo->setVisible(false);
    CursorInfo->setFont(font);
}

void qOsc::Init()
{
    Tracer1 =  new QCPItemTracer(this);
    Tracer2 =  new QCPItemTracer(this);
    CursorInfo = new QCPItemText(this);
    setTracerPropieties();
    setCursorInfoPropieties();
    this->axisRect()->insetLayout()->setInsetAlignment(0, Qt::AlignTop|Qt::AlignLeft);
    connect(this,&qOsc::mousePress,this,&qOsc::mousePressSlot);
}
void qOsc::displayData(QString name,QList<double> &timestamps_ms,QList<double> &data)
{
    const QColor ColorTable[16]={Qt::red, Qt::green,Qt::blue,Qt::cyan,Qt::magenta,
                                 Qt::yellow,Qt::darkRed,Qt::darkGreen,Qt::darkBlue,
                                 Qt::black, Qt::darkGray, Qt::gray, Qt::lightGray,
                                 Qt::darkCyan,Qt::darkMagenta,Qt::darkYellow};

    if(!SignalNames.contains(name))
    {
        QFont font;
        SignalNames.append(name);
        // ---- New Signal.Configure Axies----
        this->addGraph();
        int index = SignalNames.indexOf(name);
        this->graph(index)->setPen(QPen(QColor(ColorTable[index&0xF])));
        this->graph(index)->setData(timestamps_ms.toVector(),data.toVector());
        this->graph(index)->rescaleAxes(true);
        this->graph(index)->setName(SignalNames.last());
        this->graph(index)->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssCircle, 2));
       // Tracer->setGraph(graph((index)));
        font.setFamily("Monospace");
        font.setPointSize(8);
        this->legend->setFont(font);
        this->legend->setVisible(true);

        this->setInteractions(QCP::iRangeDrag | QCP::iRangeZoom /*| QCP::iSelectPlottables |QCP::iSelectItems*/);
    }
    else
    {
        int index = SignalNames.indexOf(name);
        this->graph(index)->setData(timestamps_ms.toVector(),data.toVector());
        this->graph(index)->rescaleAxes(true);
        this->graph(index)->rescaleKeyAxis();

    }

}

void qOsc::resetall()
{
    this->clearPlottables();
    this->clearGraphs();
    SignalNames.clear();
}

void qOsc::mousePressSlot(QMouseEvent *event)
{
    static uint8_t toggle=0;

    double x=this->xAxis->pixelToCoord(event->pos().x());
    double y=this->yAxis->pixelToCoord(event->pos().y());

    if(toggle)
    {
        Tracer1->position->setCoords(x,y);
        Tracer1->updatePosition();
    }
    else
    {
        Tracer2->position->setCoords(x,y);
        Tracer2->updatePosition();
    }
    toggle ^=1;

    double tr_x1 = Tracer1->position->key();
    double tr_y1 = Tracer1->position->value();
    double tr_x2 = Tracer2->position->key();
    double tr_y2 = Tracer2->position->value();
    CursorInfo->setText(QString("(%1, ").arg(x)+QString("%1)").arg(y)+QString("\n(dX %1, ").arg(tr_x2-tr_x1)+QString("dY %1)").arg(tr_y2-tr_y1));
    CursorInfo->position->setCoords(x,y);
    Tracer1->setVisible(true);
    Tracer2->setVisible(true);
    CursorInfo->setVisible(true);
    this->replot();
}
void qOsc::fitall()
{
    for(int cnt=0;cnt<SignalNames.length();cnt++)
    {
        this->graph(cnt)->rescaleAxes(false);
        this->graph(cnt)->rescaleKeyAxis();
    }
    this->replot();
}
