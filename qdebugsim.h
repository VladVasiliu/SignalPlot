#ifndef QDEBUGSIM_H
#define QDEBUGSIM_H

#include <QObject>
#include <QTcpSocket>

class QDebugSim : public QObject
{
    Q_OBJECT

    QTcpSocket Socket;
    QByteArray Rxbytes;
    double timestamp_start;

public:
    explicit QDebugSim(QObject *parent = nullptr);
    ~QDebugSim();

public slots:
    void Connect(int port=9999);
    void Disconnect();
    void Write(uint64_t address,uint8_t *data,uint32_t size);
    void ReadReq(uint64_t address,uint32_t size);
    void ReadFromSocket();
signals:
    void resetData();
    uint32_t updateRawData(uint64_t timestamp,uint64_t address,uint8_t *data,uint32_t size);
    void Log(QString text);

};

#endif // QDEBUGSIM_H
