#ifndef QROUTER_H
#define QROUTER_H

#include <QObject>
#include "qopenocd.h"
#include "qdebugsim.h"
#include "qupdi.h"
#include "qcandaq.h"


class qRouter : public QObject
{
    Q_OBJECT
public:
    enum tenConnectionType
    {
      qNoConnection=0,
      qOpenOCDConnection,
      qDebugSimulation,
      qAvrUdpi,
      qCanDaq
    };
private:
    tenConnectionType ConnectionType = qNoConnection;
    QOpenOCD  OpenOCD;
    QDebugSim DebugSim;
    qUPDI     UPDI;
    qcandaq   CanDaq;
public:
    explicit qRouter(QObject *parent = nullptr);
    ~qRouter();


public slots:
    void Connect(QString port,tenConnectionType type = qOpenOCDConnection);
    void Disconnect();
    void Write(uint64_t address,uint8_t *data,uint32_t size);
    void ReadReq(uint64_t address,uint32_t size);

    void resetData_In();
    uint32_t updateRawData_In(uint64_t timestamp,uint64_t address,uint8_t *data,uint32_t size);
    void LogIn(QString text);

signals:
    void resetData();
    uint32_t updateRawData(uint64_t timestamp,uint64_t address,uint8_t *data,uint32_t size);
    void Log(QString text);

};

#endif // QROUTER_H
