#include "qrouter.h"

qRouter::qRouter(QObject *parent) : QObject(parent)
{

}

qRouter::~qRouter()
{
    Disconnect();
}

void qRouter::Connect(QString port, tenConnectionType type)
{

    if(ConnectionType != qNoConnection) return ;

    ConnectionType =  type;

    switch (ConnectionType)
    {
    case qOpenOCDConnection:
    {
        connect(&OpenOCD,&QOpenOCD::Log,this,&qRouter::LogIn);
        connect(&OpenOCD,&QOpenOCD::resetData,this,&qRouter::resetData_In);
        connect(&OpenOCD,&QOpenOCD::updateRawData,this,&qRouter::updateRawData_In);
        OpenOCD.Connect(port.toInt(nullptr,10));

    }break;
    case qDebugSimulation:
    {
        connect(&DebugSim,&QDebugSim::Log,this,&qRouter::LogIn);
        connect(&DebugSim,&QDebugSim::resetData,this,&qRouter::resetData_In);
        connect(&DebugSim,&QDebugSim::updateRawData,this,&qRouter::updateRawData_In);
        DebugSim.Connect(port.toInt(nullptr,16));

    }break;
    case qAvrUdpi:
    {
        connect(&UPDI,&qUPDI::Log,this,&qRouter::LogIn);
        connect(&UPDI,&qUPDI::resetData,this,&qRouter::resetData_In);
        connect(&UPDI,&qUPDI::updateRawData,this,&qRouter::updateRawData_In);
        UPDI.Connect(port);

    }break;
    case qCanDaq:
    {
        connect(&CanDaq,&qcandaq::Log,this,&qRouter::LogIn);
        connect(&CanDaq,&qcandaq::resetData,this,&qRouter::resetData_In);
        connect(&CanDaq,&qcandaq::updateRawData,this,&qRouter::updateRawData_In);
        CanDaq.Connect(port);

    }break;
    default:
    {
        emit Log("ERROR : No connection method was chosen !");
        ConnectionType = qNoConnection;
    }
    }

}
void qRouter::Disconnect()
{
    switch (ConnectionType)
    {
    case qOpenOCDConnection:
    {
        OpenOCD.Disconnect();
        disconnect(&OpenOCD,&QOpenOCD::Log,this,&qRouter::LogIn);
        disconnect(&OpenOCD,&QOpenOCD::resetData,this,&qRouter::resetData_In);
        disconnect(&OpenOCD,&QOpenOCD::updateRawData,this,&qRouter::updateRawData_In);
        ConnectionType = qNoConnection;
    }break;
    case qDebugSimulation:
    {
        DebugSim.Disconnect();
        disconnect(&DebugSim,&QDebugSim::Log,this,&qRouter::LogIn);
        disconnect(&DebugSim,&QDebugSim::resetData,this,&qRouter::resetData_In);
        disconnect(&DebugSim,&QDebugSim::updateRawData,this,&qRouter::updateRawData_In);
        ConnectionType = qNoConnection;

    }break;
    case qAvrUdpi:
    {
        UPDI.Disconnect();
        disconnect(&UPDI,&qUPDI::Log,this,&qRouter::LogIn);
        disconnect(&UPDI,&qUPDI::resetData,this,&qRouter::resetData_In);
        disconnect(&UPDI,&qUPDI::updateRawData,this,&qRouter::updateRawData_In);
        ConnectionType = qNoConnection;

    }break;
    case qCanDaq:
    {
        CanDaq.Disconnect();
        disconnect(&CanDaq,&qcandaq::Log,this,&qRouter::LogIn);
        disconnect(&CanDaq,&qcandaq::resetData,this,&qRouter::resetData_In);
        disconnect(&CanDaq,&qcandaq::updateRawData,this,&qRouter::updateRawData_In);
        ConnectionType = qNoConnection;
    }break;
    default:
    {
        emit Log("ERROR : No connection method was chosen !");
        ConnectionType = qNoConnection;
    }
    }
}
void qRouter::Write(uint64_t address,uint8_t *data,uint32_t size)
{
    switch (ConnectionType)
    {
    case qOpenOCDConnection:
    {
        OpenOCD.Write(address,data,size);
    }break;
    case qDebugSimulation:
    {
        DebugSim.Write(address,data,size);
    }break;
    case qAvrUdpi:
    {
        UPDI.Write(address,data,size);
    }break;
    case qCanDaq:
    {
        CanDaq.Write(address,data,size);
    }break;
    default:
    {
        emit Log("ERROR : No connection method was chosen !");
        ConnectionType = qNoConnection;
    }
    }
}
void qRouter::ReadReq(uint64_t address,uint32_t size)
{
    switch (ConnectionType)
    {
    case qOpenOCDConnection:
    {
        OpenOCD.ReadReq(address,size);
    }break;
    case qDebugSimulation:
    {
        DebugSim.ReadReq(address,size);
    }break;
    case qAvrUdpi:
    {
        UPDI.ReadReq(address,size);
    }break;
    case qCanDaq:
    {
        CanDaq.ReadReq(address,size);
    }break;
    default:
    {
        emit Log("ERROR : No connection method was chosen !");
        ConnectionType = qNoConnection;
    }
    }
}


void qRouter::resetData_In()
{
    emit resetData();
}
uint32_t qRouter::updateRawData_In(uint64_t timestamp,uint64_t address,uint8_t *data,uint32_t size)
{
     return emit updateRawData(timestamp,address,data,size);
}
void qRouter::LogIn(QString text)
{
    emit Log(text);
}
