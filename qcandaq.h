#ifndef QCANDAQ_H
#define QCANDAQ_H

#include <QObject>
#include <QTimer>

#include <cansoket.h>
#include "semaphore.h"

struct qcanrequest
{
    uint8_t ID;
    uint8_t dlc;
    uint8_t data[8];
};

class qcandaq : public QObject
{
    Q_OBJECT
    std::list<qcanrequest> requests;
    semaphore  sema;
    QTimer RxTimeoutTimer;
    QString CanSocketName;
    cansoket sock;
    double timestamp_start=0;
    int TxIDReadReq  = 1;
    int TxIDWriteReq = 2;
    int RxID = 3;
    unsigned long  MaxReqBufferLen = 100000;
    void AddRequest(uint8_t ID,uint8_t dlc,uint8_t data[8]);
public:
    explicit qcandaq(QObject *parent = nullptr);

public slots:
    void Connect(QString port);
    void Disconnect();
    void Write(uint64_t address,uint8_t *data,uint32_t size);
    void ReadReq(uint64_t address,uint32_t size);

    void readyRead();
    void ReqTimeout();


signals:
    void resetData();
    uint32_t updateRawData(uint64_t timestamp,uint64_t address,uint8_t *data,uint32_t size);
    void Log(QString text);
};

#endif // QCANDAQ_H
