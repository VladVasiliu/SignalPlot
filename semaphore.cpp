#include "semaphore.h"


semaphore::semaphore()
{
    count=0;
}

void semaphore::release()
{
    std::lock_guard<decltype(mutex_)> lock(mutex_);
    if(count > 0)
    {
        count--;
    }
    condition.notify_one();
}

void semaphore::acquire()
{
    std::unique_lock<decltype(mutex_)> lock(mutex_);
    while(count > 0)
    { // Handle spurious wake-ups.
        condition.wait(lock);
    }
    count++;
}

bool semaphore::try_acquire()
{
    std::lock_guard<decltype(mutex_)> lock(mutex_);
    if(count==0)
    {
        count++;
        return true;
    }
    return false;
}
