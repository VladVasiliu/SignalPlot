#include "qupdi.h"

#include <QDateTime>
#include <QTime>

qUPDI::qUPDI(QObject *parent)
{

}

qUPDI::~qUPDI()
{
    Disconnect();
}
void qUPDI::UARTTxRequest(QByteArray &data)
{
    struct timespec ts = { 5 / 1000, (5 % 1000) * 1000 * 1000 };
    serial.write(data);
    serial.flush();

    foreach(char i,data)
    {
        /* ---- Keep track of all the bytes transmited ----*/
        TxRxExpectedData.append(i);
        TxRxOperation.append(UART_Tx);
    }

    nanosleep(&ts, NULL);
}
void qUPDI::UARTRxRequest()
{
    /* ---- Keep track of all the bytes expected to be received  ----*/
    TxRxExpectedData.append((char)0);
    TxRxOperation.append(UART_Rx);
}

void qUPDI::ErrorHandler()
{

    QString PortName;
    emit Log("ERROR : UPDI Communication Error. Trying to restart the UPDI communication");

    PortName = serial.portName();

    Disconnect();
    Connect(PortName);

}

void qUPDI::WriteReq8(uint64_t address, uint8_t val)
{
    QByteArray buffer;

    buffer.append(UDPI_SYNC);
    buffer.append(UDPI_STS_OPCODE);
    buffer.append((address>>0u)&0xFF);
    buffer.append((address>>8u)&0xFF);
    UARTTxRequest(buffer);
    UARTRxRequest(); /* Expect an ack */
    buffer.clear();
    buffer.append(val);
    UARTTxRequest(buffer);
    UARTRxRequest();/* Expect an ack */
}

void qUPDI::ReadReq8(uint64_t address)
{
    QByteArray buffer;

    buffer.append(UDPI_SYNC);
    buffer.append(UDPI_LDS_OPCODE);
    buffer.append((address>>0u)&0xFF);
    buffer.append((address>>8u)&0xFF);
    UARTTxRequest(buffer);
    UARTRxRequest();/* Expect an ack */
}




void qUPDI::ResetComm()
{
    bool restart = false;

    if(serial.isOpen())
    {
        restart = true;
        serial.close();
        RxData.clear();
        TxRxExpectedData.clear();
        TxRxOperation.clear();
    }

    serial.setBaudRate(300);
    serial.setBreakEnabled(false);
    serial.setDataBits(QSerialPort::DataBits::Data8);
    serial.setParity(QSerialPort::Parity::EvenParity);
    serial.setFlowControl(QSerialPort::FlowControl::NoFlowControl);
    serial.setStopBits(QSerialPort::StopBits::OneStop);

    if(serial.open(QIODevice::ReadWrite))
    {
        QByteArray buffer;

        buffer.append((char)UDPI_BREAK);

        emit Log ("INFO : Reset UDPI!\n");
        struct timespec ts = { 100 / 1000, (100 % 1000) * 1000 * 1000 };
        /* ---- Transmit sync pulse ---- */
        UARTTxRequest(buffer);
        nanosleep(&ts, NULL);
        /* ---- Transmit sync pulse ---- */
        UARTTxRequest(buffer);
        nanosleep(&ts, NULL);
        serial.close();

        RxData.clear();
        TxRxExpectedData.clear();
        TxRxOperation.clear();

    }
    else
    {
        emit Log ("ERROR : Unable to open the Serial port !\n");
    }



    ConfigureComm();

    if(restart == true)
    {

        if(serial.open(QIODevice::ReadWrite))
        {

        }
        else
        {
            emit Log ("ERROR : Unable to open the Serial port !\n");
        }
    }

}
void qUPDI::ConfigureComm()
{

    serial.reset();
    serial.setBaudRate(115200);
    serial.setBreakEnabled(false);
    serial.setDataBits(QSerialPort::DataBits::Data8);
    serial.setParity(QSerialPort::Parity::EvenParity);
    serial.setFlowControl(QSerialPort::FlowControl::NoFlowControl);
    serial.setStopBits(QSerialPort::StopBits::TwoStop);

}
void qUPDI::readyRead()
{
    uint8_t operation=0;

    RxData.append(serial.readAll());
    //emit Log(QString("INFO : Buffer %1 bytes long").arg(RxData.length()));


    while((RxData.length()>3)&&(TxRxExpectedData.length()>3)&&(TxRxOperation.length()>3))
    {
        if((RxData[0] == TxRxExpectedData[0])&&(TxRxOperation[0] == (char)UART_Tx)&&
           (RxData[1] == TxRxExpectedData[1])&&(TxRxOperation[1] == (char)UART_Tx))
        {
            if(RxData[0] == (char)UDPI_SYNC)
            {
                operation = RxData[1];

                /* ----the next byte must be a goodie ----*/
                switch(operation)
                {
                case 0x80:
                {
                    emit Log("INFO : UPDI : STATUSA =  "+QString("%1").arg((short)RxData[2],0,16));
                    RxData.remove(0,3);
                    TxRxExpectedData.remove(0,3);
                    TxRxOperation.remove(0,3);

                }break;
                case 0x81:
                {
                    emit Log("INFO : UPDI : STATUSB =  "+QString("%1").arg((short)RxData[2],0,16));
                    RxData.remove(0,3);
                    TxRxExpectedData.remove(0,3);
                    TxRxOperation.remove(0,3);
                }break;
                case UDPI_LDS_OPCODE :
                {
                    if(RxData.length() > 4)
                    {
                        uint32_t address =0;
                        uint8_t data=0;
                        address |= (((uint32_t)((uint8_t)RxData[2]))<<0);
                        address |= (((uint32_t)((uint8_t)RxData[3]))<<8);
                        address |= 0x800000;
                        data     = RxData[4];
                        uint64_t timestamp = QDateTime::currentMSecsSinceEpoch() - timestamp_start;
                       // emit Log("INFO : UPDI : Read Address "+QString("[%1]").arg(address,0,16));
                        emit updateRawData(timestamp,address,(uint8_t *)&data,1);
                        RxData.remove(0,5);
                        TxRxExpectedData.remove(0,5);
                        TxRxOperation.remove(0,5);
                    }
                    else
                    {
                        return;
                    }
                }break;
                case UDPI_STS_OPCODE :
                {
                    if(RxData.length() > 7)
                    {
                         RxData.remove(0,7);
                         TxRxExpectedData.remove(0,7);
                         TxRxOperation.remove(0,7);
                    }
                    else
                    {
                        return;
                    }

                }break;
                default:
                {
                    RxData.remove(0,1);
                    TxRxExpectedData.remove(0,1);
                    TxRxOperation.remove(0,1);
                }
                }
            }
            else
            {
                RxData.remove(0,1);
                TxRxExpectedData.remove(0,1);
                TxRxOperation.remove(0,1);
            }

        }
        else
        {
            /* ---- Buffers are not in sync ----*/
            //emit Log("ERROR : UPDI : Unexpected data : \n"+QString("[0]0x%1 != ").arg((short)RxData[0],0,16)+QString("0x%1").arg((short)TxRxExpectedData[0],0,16));
           // emit Log("ERROR : UPDI : Unexpected data : \n"+QString("[1]0x%1 != ").arg((short)RxData[1],0,16)+QString("0x%1").arg((short)TxRxExpectedData[1],0,16));

            /* ---- Drop the bytes until a SYNC -----*/
            while((TxRxExpectedData.length()>0)&&(TxRxOperation.length()>0))
            {
                if(TxRxExpectedData[0] != (char)UDPI_SYNC)
                {
                   TxRxExpectedData.remove(0,1);
                   TxRxOperation.remove(0,1);
                }
                else
                {
                    break;
                }
            }
            /* ---- Try to resync  the RX buffer and Drop the bytes until a SYNC---- */
            while(RxData.length() >0)
            {
                if(RxData[0] != (char)UDPI_SYNC)
                {
                    RxData.remove(0,1);
                }
                else
                {
                    break;
                }
            }
            /* ---- Perform the Error handling ----*/
            ErrorHandler();
        }
    }


}

void qUPDI::Connect(QString port)
{
    bool ok = false;
    QByteArray buffer;

    serial.setPortName(port);
    serial.clear();
    serial.clearError();
    RxData.clear();
    TxRxExpectedData.clear();
    TxRxOperation.clear();

    connect(&serial,&QSerialPort::readyRead,this,&qUPDI::readyRead);

    if(serial.open(QIODevice::ReadWrite))
    {

        emit Log("INFO : UPDI : Connected to "+port);

        /* ---- Transmit sync pulse ---- */
        buffer.clear();
        buffer.append((uint8_t)UDPI_BREAK);
        buffer.append(UDPI_SYNC);
        buffer.append(0xC3);
        buffer.append(0x08);

        buffer.append(UDPI_SYNC);
        buffer.append(0xC2);
        buffer.append(0x10);


        buffer.append(UDPI_SYNC);
        buffer.append(0x80);

        UARTTxRequest(buffer);
        UARTRxRequest(); /* Expect UPDI : STATUSA*/
        buffer.clear();

        buffer.append(UDPI_SYNC);
        buffer.append(0x81);

        UARTTxRequest(buffer);
        UARTRxRequest(); /* Expect UPDI : STATUSB*/

        buffer.clear();


        if(ok ==false)
        {
            emit Log("INFO : UDPI : Reset Connection");
            ResetComm();
            buffer.clear();

            /* ---- Transmit sync pulse ---- */
            buffer.append(UDPI_SYNC);
            buffer.append(0xC3);
            buffer.append(0x08);

            buffer.append(UDPI_SYNC);
            buffer.append(0xC2);
            buffer.append(0x10);

            buffer.append(UDPI_SYNC);
            buffer.append(0x80);

            UARTTxRequest(buffer);
            UARTRxRequest(); /* Expect UPDI : STATUSA*/
            buffer.clear();


            buffer.append(UDPI_SYNC);
            buffer.append(0x81);

            UARTTxRequest(buffer);
            UARTRxRequest(); /* Expect UPDI : STATUSB*/
            buffer.clear();

        }

        timestamp_start = QDateTime::currentMSecsSinceEpoch();
    }
    else
    {
        emit Log("ERROR : UPDI : Failed to Connect to "+port);
    }
}

void qUPDI::Disconnect()
{
    if(serial.isOpen())
    {
        QByteArray buffer;
        /* ---- Disable the Debug interface -----*/
        buffer.append(UDPI_SYNC);
        buffer.append(0xC3);
        buffer.append(0x04);
        UARTTxRequest(buffer);
        buffer.clear();

        serial.close();

        emit Log("INFO : UPDI : Serial UART Connection Closed");
        disconnect(&serial,&QSerialPort::readyRead,this,&qUPDI::readyRead);
        RxData.clear();
        TxRxExpectedData.clear();
        TxRxOperation.clear();
    }
}

void qUPDI::Write(uint64_t address, uint8_t *data, uint32_t size)
{
    uint64_t count=0;

    if(serial.isOpen())
    {
        for(count=0;count<size;count++)
        {
            WriteReq8(address+count,data[count]);
        }

    }

}

void qUPDI::ReadReq(uint64_t address, uint32_t size)
{
    uint64_t count=0;
    if(serial.isOpen())
    {
        for(count=0;count<size;count++)
        {
            ReadReq8(address+count);
        }

    }
}
