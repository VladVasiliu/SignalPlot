#ifndef CANSOKET_H
#define CANSOKET_H

#include <QString>
#include <QSocketNotifier>
class cansoket :public QObject
{
    Q_OBJECT

    int skt = -1;
    QSocketNotifier *SktNotifier= NULL;
public:
    cansoket();
    ~cansoket();
    void canInit(const char *name);
    void canSetLoopback(bool loopback);
    int  canTx(int ID,int DLC, unsigned char *data);
    int  canRx(int &MAXDLC,unsigned char *data);
    QString LastError;
    bool canIsOpened(void);
    void canClose(void);

public slots:
    void dataReceived();
signals:
    void dataAvailable();
};

#endif // CANSOKET_H
