/*Copyright (C) 2019  Vlad Vasiliu

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/
#ifndef QOSC_H
#define QOSC_H

#include <qcustomplot.h>
#include <QObject>
#include <QWidget>

class qOsc : public QCustomPlot
{
    Q_OBJECT
    QList<QString> SignalNames;
    QCPItemTracer  *Tracer1=nullptr;
    QCPItemTracer  *Tracer2=nullptr;
    QCPItemText    *CursorInfo=nullptr;

    void setTracerPropieties();
    void setCursorInfoPropieties();
    void Init();

public:
    qOsc():QCustomPlot(){Init();}
    qOsc(QWidget*&w):QCustomPlot(w){Init();}
    qOsc(const qOsc &ref):QCustomPlot(nullptr)
    {
        SignalNames = ref.SignalNames;
        Init();
    }
    // ---- Necessary operators for a list ------
    qOsc operator = (const qOsc &ref){
        SignalNames = ref.SignalNames;
        CursorInfo=ref.CursorInfo;
        return ref;
    }

    QList<QString> &getSignals() {return SignalNames;}

public slots:

    void displayData(QString name,QList<double> &timestamps_ms,QList<double> &data);
    void update(){this->replot();}
    void resetall();
    void fitall();
    void mousePressSlot(QMouseEvent *event);
};

#endif // QOSC_H
