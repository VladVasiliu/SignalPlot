/*Copyright (C) 2019  Vlad Vasiliu

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/
#include "qinterpreter.h"

qInterpreter::qInterpreter(QObject *parent) : QObject(parent)
{

}
void qInterpreter::Decode(QString cmd)
{
    double val=0;

    if(cmd.contains("="))
    {
        QStringList str = cmd.split('=');
        emit Log("INFO : Set variable "+ cmd.simplified());
        emit SetValue(str[0].simplified(),str[1].simplified().toDouble());

    }
    else
    {
        val = emit GetValue(cmd.simplified());
        emit Log("INFO : "+cmd.simplified()+" = "+QString("%1").arg(val)+QString("[hex : 0x%1]").arg((long)val,0,16));
    }


}
