# SignalPlot

Variable Plotter which uses OpenOCD / SimServer. You can check the wiki pages(https://gitlab.com/VladVasiliu/SignalPlot/-/wikis/home) for more informations 

## Usecases
### Monitoring variables present on a uC and access with Open OCD is possible.
1. Be sure that the OpenOCD  is started and the telnet port is 6666 (default). (This tool uses the Telnet to allow gdb to work  in paralel)
2. Start the SignalPlot and be sure that connection type and port is as expected.(Connection Configuration tab)
3. In the measurement Configuration tab add using the input fields the variables.
4. When used together with symboldecoder the information regarding type and address are not necessary.Use "File->Use Elf" menu entry to select the used elf file  and the address and the type will be automatically updated.The information will be also shown in the Console view.
5. To connect to the OpenOcd just select Connection Configuration tab -> Connect button 
6. To monitor a signal right click on the variable in the variable list and select 
    - "Add to new figure " or
    - "Add to the last added figure"  if one already exists .
    - All the graphics are numerotated so you can add also to a specific graphic view .
7. Sampling time can be changed using the Sample Time (s)  input 

### Monitoring variables present in a Simulated environment. (Simserver)
1. The usage is the same like in the openOCD  but the connection type shall be changed.
2. Instead of monitoring a uC variables, variables from a separate process called DUT (device under Test)  can be visualised .


## Open points
    - A drag and drop will be nice. Currently  no drag and drop is implemented . 

## Dependencies 

Qt
qcustomplot (a copy is placed in the repository and shares the GPL3 Licence)
symboldecoder -> https://gitlab.com/VladVasiliu/symboldecoder.git
OpenOCD ->  when is used with OpenOCD

The symboldecoder  must be installed (copied in bin folder or the path must be included in $PATH )
Without this, the SignalPlot is hard to use and not that useful.


## Building information
Install symboldecoder > https://gitlab.com/VladVasiliu/symboldecoder.git

```
git clone https://gitlab.com/VladVasiliu/SignalPlot.git
cd SignalPlot 
make 
```

To run the SignalPlot (in the SignalPlot folder ):
```
./SignalPlot

``` 
## Disclaimer 
The software could include technical or other mistakes, inaccuracies or errors. The software may be out of date, and I make no commitment to update it. Use it on your own risk.


