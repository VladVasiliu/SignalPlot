/*Copyright (C) 2019  Vlad Vasiliu

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/
#ifndef SIGNALPLOTWINDOW_H
#define SIGNALPLOTWINDOW_H

#include <QMainWindow>
#include <QFileDialog>
#include "qosc.h"
#include "qvariable.h"
#include "qrouter.h"
#include "qconfig.h"
#include "qinterpreter.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class SignalPlotWindow : public QMainWindow
{
    Q_OBJECT

    QTimer   EventTrigger;
    QTimer   GuiTimer;
    qRouter  ConnectionRouter;
    QList<qVariable> VariableList;
    QString elffile;
    qConfig  config;
    QList<qOsc> qOscViewList;
    int         OscActiveView;
    qInterpreter Interpreter;
    int          Meas_ColNr=2;

    bool isVariableInputOk(QString &name,int &type,uint64_t &ui64addr,double &flgain,double &floffset);
    bool isVarInMeasurementList(QString &name);
    void updateVariableInput(QString name);
    void updateLayout(int nrCol);
    int  calculateBufferSize();
public:
    SignalPlotWindow(QWidget *parent = nullptr);
    ~SignalPlotWindow();
public slots:
    void Logger(QString text);
    void SetVariable(QString name,int type,uint64_t address,double gain,double offset);
    void SetElf(QString data);
    void AddMeasurement(int view);
    void AddMeasurementNewAxis();
    void RemoveMeasurement();
    void RemoveGraph();
    void FitGraph();

    void ResetOscViews();
    void ConnectToNewOscView(qVariable &var);
    void ConnectToOscView(qVariable &var);
    void DisconnectFromOscView(qVariable &var);
    void RemoveSelectedOscView();

    void   SetVarValue(QString variable,double data);
    double GetVarValue(QString variable);


    void startTimers();
    void stopTimers();

    void updateValues();

signals:
    void ElfUpdate(QString &elffile);

    void resetData();


private slots:
    void on_actionUse_Elf_triggered();

    void on_actionOpen_triggered();

    void on_Add_Variable_clicked();

    void on_GetFromElf_clicked();

    void on_UpdateVar_clicked();

    void on_RemoveVar_clicked();

    void on_OpenOCDConnect_clicked();

    void on_OpenOCDDisconnect_clicked();

    void on_BufferSizeInput_editingFinished();

    void on_actionUpdate_Addresses_triggered();

    void on_actionSave_triggered();

    void on_actionNew_triggered();

    void on_varlist_customContextMenuRequested(const QPoint &pos);

    void on_qOscContextMenuRequestedSlot(const QPoint &pos);


    void on_Cmd_editingFinished();

    void on_actionSave_Data_as_CSV_triggered();

    void on_TriggerPeriod_editingFinished();

    void on_ConnectionMethod_currentIndexChanged(int index);

    void on_varlist_itemClicked(QTreeWidgetItem *item, int column);

    void on_Cmd_returnPressed();

private:
    Ui::MainWindow *ui;
};
#endif // SIGNALPLOTWINDOW_H
